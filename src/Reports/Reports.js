import React from 'react';

import TextComponent from '../Components/TextComponent';

const Reports = () => {
    return(
        <section id="reports">
        <div className="container">
            <div className="row">
                <div className="col-lg-6 d-flex justify-content-md-end">
                        <TextComponent 
                        className = 'sub-headline'
                        headline="Generate Detailed Reports Instantly"
                        details="Automatically generate in-detailed reports with the simple click of a button. Whether you
                            want instant purchase / product or complete inventory report."
                        />
                </div>
                <div className="col-lg-6">
                    <div className="report-block">
                        <h4>Items Reports</h4>
                        <div className="row">
                            <div className="col">
                                <div className="table-head">
                                    <div className="head d-flex justify-content-center align-items-center">
                                        <div></div>
                                        <div></div>
                                    </div>
                                    <div className="head d-flex justify-content-center align-items-center">
                                        <div></div>
                                        <div></div>
                                    </div>
                                    <div className="head d-flex justify-content-center align-items-center">
                                        <div></div>
                                        <div></div>
                                    </div>
                                    <div className="d-flex justify-content-end">
                                        <div className="small-head d-flex justify-content-center align-items-center">
                                            <div></div>
                                        </div>
                                        <div className="small-head d-flex justify-content-center align-items-center">
                                            <div></div>
                                        </div>
                                    </div>
                                </div>

                                <div className="table-body">
                                    <div className="body-row d-flex justify-content-center align-items-center">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                    <div className="body-row d-flex justify-content-center align-items-center">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                    <div className="body-row d-flex justify-content-center align-items-center">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                    <div className="body-row d-flex justify-content-center align-items-center">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    )
}
export default Reports;