import React from 'react';
import Input from '../Components/Input';
import TextComponent from '../Components/TextComponent';

const Import = () => {
    return(
        <section id="import">
        <div className="container">
            <div className="row">
                <div className="col-lg-6 d-flex justify-content-md-end">
                        <TextComponent 
                        className = 'sub-headline'
                        headline = "Import New Products To Inventory"
                        details = "With the built-in option that helps you to import your products to your inventory using CSV file you can a lot of time and effort so you don’t waste them on repetitive tasks."
                        />
                </div>
                <div className="col-lg-6">
                    <div className="import-box">
                        <h4>Import CSV</h4>
                        <form>
                            <div className="form-group row">
                                <label for="" className="col-sm-4 col-form-label">Type</label>
                                <div className="col-sm-8">
                                    <select className="form-control">
                                        <option>Contact</option>
                                        <option>Company</option>
                                        <option>Employee</option>
                                        <option>Product</option>
                                    </select>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label for="" className="col-sm-4 col-form-label">CSV File</label>
                                <div className="col-sm-8">
                                <Input 
                                inputType = 'file'
                                />
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="offset-4 col-sm-8 d-flex justify-content-start">
                                    <button type="submit" className="btn btn-white">Import</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    )
}

export default Import;