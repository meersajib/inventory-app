import React from 'react';
import SeacchIcon from '../img/search.png';

import TextComponent from '../Components/TextComponent';
import Input from '../Components/Input';

const Search = () => {
    return(
        <section id="search">
        <div className="container">
            <div className="row">
                <div className="col-lg-6 d-flex justify-content-md-center">
                    <div className="item-block">
                        <div className="item-icon d-flex justify-content-center align-items-center"><img src={SeacchIcon}
                                className="search-img" alt="" /></div>
                        <Input 
                        className = 'search-input'
                        inputType='text'
                        placeHolder = 'Search Product'
                        />        
                    </div>
                </div>
                <div className="col-lg-6">
                    <div className="sub-headline">
                        <TextComponent headline="Search Whatever, Whenever You Want"
                        details = "With the powerful built in search feature with filtering options, you don’t have to worry about not finding the product that you are looking for in time."
                        />
                    </div>
                </div>
            </div>
        </div>
    </section>
    )
}
export default Search;