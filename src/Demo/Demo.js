import React from 'react';
import Button from '../Components/Button';

import bigFile from '../img/big-file.png';

const Demo = () => {
    return(
        <section id="demo">
        <div className="container">
            <div className="row">
                <div className="col-lg-2 d-flex justify-content-md-end">
                    <img src={bigFile} alt="" />
                </div>
                <div className="col-lg-6">
                    <h1>Experience easy stock & merchandise management for your products.</h1>
                    <p>We know talks are cheap. So we made an official demo of our product so you can have an hand-on experience</p>
                </div>
                <div className="col-lg-4 d-flex justify-content-center align-items-center">    
                <Button 
                    className='demo-btn btn'
                    btnText='Go to Demo'
                    />
                </div>
            </div>
        </div>
    </section>
    )
}

export default Demo;