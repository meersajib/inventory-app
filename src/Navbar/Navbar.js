import React from 'react';
import logo from '../img/logo.png';


const Navbar = () => {
    return(
        <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand">
                    <img src={logo} alt="" />
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="#">Tour <span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Pricing</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link " href="#">Extensions</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link " href="#">Get Help</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link " href="#">Download</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link " href="#">Join Free</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link " href="#">Sign in</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    )
}
export default Navbar;