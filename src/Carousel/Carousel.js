import React from 'react';

import Button from '../Components/Button';
import Navbar from '../Navbar/Navbar';
import fileImage from '../img/file-manager.png';

import versionImg from '../img/version.png';
import typeImg from '../img/type.png';

const carouselContent = [
    {
        price: "79$",
        description: "Single Sites"
    },
    {
        price: "169$",
        description: "Upto 3 Sites"
    },
    {
        price: "719$",
        description: "Upto 10 Sites"
    },
    {
        price: "399$",
        description: "Upto 50 Sites"
    }
]

const CarouselCard = props => {
    return (
        <div className="price-box py-4 text-center">
            <h6>{props.price}</h6>
            <p>{props.description}</p>
        </div>
    )
}

const Carousel = (props) => {
    return (
        <section id="carousel">
            <span className="skewed">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </span>
            <div className="container-fluid absolute-position">
            <Navbar/>
                <div className="row no-gutter">
                    <div className="col-lg-6">
                        <div className="d-flex justify-content-md-end">
                            <div className="carousel-img-card">
                                <div className="carousel-img-bg d-flex justify-content-center py-5">
                                    <img src={fileImage} className="img-violet" alt="" />
                                </div>
                                <div className="carousel-img-card-footer d-flex justify-content-start align-items-center">
                                    <div className="d-50"><img src={versionImg} /><p>Version :<span>1.01</span></p></div>
                                    <div className="d-50"><img src={typeImg} /><p>Type :<span>Accounting</span></p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6 d-flex justify-content-start">
                        <div className="py-5">
                            <div className="car-head"><h3>Organize All Your Stock Products Right From Your WordPress Site</h3>
                            <p>Stay ahead of your finished stocks & goods with the Must Have WordPress Inventory Management
                            plugin.</p></div>
                            <div className="card-holder">
                            <CarouselCard price={carouselContent[0].price} description={carouselContent[0].description} />
                            <CarouselCard price={carouselContent[1].price} description={carouselContent[1].description} />
                            <CarouselCard price={carouselContent[2].price} description={carouselContent[2].description} />
                            <CarouselCard price={carouselContent[3].price} description={carouselContent[3].description} />
                            </div>
                            <div className="clr"></div>
                            <div className="d-flex justify-content-md-start my-3 tab-center">
                            <Button 
                            className='btn purchase-btn'
                            btnText="Purchase Now"
                            />
                                <a href="" className="mute-text pt-2 pl-4">View Changelog</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default Carousel;