import React from 'react';
import ScreenshotImg from '../img/screenshot.png';

const Screenshot = () => {
    return (
        <section id="screen-shot">
            <div className="container-fluid">
                <div className="row py-5">
                    <div className="col">
                        <div className="section-headline text-center">
                            <h2>Screenshot</h2>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-4 d-flex justify-content-center justify-content-md-end">
                            <div className="img-frame d-flex justify-content-center align-items-center">
                                <img src={ScreenshotImg} alt="" />
                            </div>
                        </div>
                        <div className="col-md-4 d-flex justify-content-center">
                            <div className="img-frame d-flex justify-content-center align-items-center">
                                <img src={ScreenshotImg} alt="" />
                            </div>
                        </div>
                        <div className="col-md-4 d-flex justify-content-center justify-content-md-start">
                            <div className="img-frame d-flex justify-content-center align-items-center">
                                <img src={ScreenshotImg} alt="" />
                            </div>
                        </div>
                    </div>
                    <div className="row py-5">
                        <div className="col-md-4 d-flex justify-content-center justify-content-md-end">
                            <div className="img-frame d-flex justify-content-center align-items-center">
                                <img src={ScreenshotImg} alt="" />
                            </div>
                        </div>
                        <div className="col-md-4 d-flex justify-content-center">
                            <div className="img-frame d-flex justify-content-center align-items-center">
                                <img src={ScreenshotImg} alt="" />
                            </div>
                        </div>
                        <div className="col-md-4 d-flex justify-content-center justify-content-md-start">
                            <div className="img-frame d-flex justify-content-center align-items-center">
                                <img src={ScreenshotImg} alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default Screenshot;