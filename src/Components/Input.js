import React from 'react';

const Input = (props) => {
    return(
        <input type={props.inputType} placeholder={props.placeHolder} className={props.className} />
    )
}
export default Input;