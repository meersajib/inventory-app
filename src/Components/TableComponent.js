import React from 'react';

const TableComponent = () => {
    return (
        <table>
            <thead>
                <tr>
                    <th className="pl-4">Name</th>
                    <th></th>
                    <th>Item Code</th>
                    <th>Cost</th>
                    <th>Sell</th>
                    <th>
                        <div className="point"></div>
                        <div className="point"></div>
                        <div className="point"></div>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td className="d-flex mx-auto my-4 justify-content-center align-items-center">
                        <div className="mini-box"></div>
                    </td>
                    <td>
                        <div></div>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                </tr>
                <tr>
                    <td className="d-flex mx-auto my-4 justify-content-center align-items-center">
                        <div className="mini-box"></div>
                    </td>
                    <td>
                        <div></div>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                </tr>
                <tr>
                    <td className="rowThreeColOne d-flex mx-auto my-4 justify-content-center align-items-center">
                        <div className="mini-box"></div>
                    </td>
                    <td>
                        <div></div>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                    <td>
                        <div></div>
                    </td>
                </tr>
            </tbody>
        </table>
    )
}

export default TableComponent;