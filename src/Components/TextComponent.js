import React from 'react';


const TextComponent = (props) => {
    return (
        <div>
            <div className={props.className}>
                <h3>{props.headline}</h3>
                <p>{props.details}</p>
            </div>
        </div>
    )
}

export default TextComponent;