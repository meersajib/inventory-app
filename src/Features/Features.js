import React from 'react';
import TextCompoent from '../Components/TextComponent';
import TableComponent from '../Components/TableComponent';
import file from '../img/file-icon.png';


const Features = () => {
    return (
        <section id="features">
            <div className="container">
                <div className="row clr">
                    <div className="col negative-margin text-center">
                        <div className="img-circle d-flex align-items-center text-center py-4 m-auto">
                            <img className="m-auto" src={file} alt="" />
                        </div>
                        <div className="section-headline">
                        <h2>Features</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing Lorem Ipsum has been the industry</p>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-6 d-flex justify-content-md-end">
                        <TableComponent />
                    </div>
                    <div className="col-lg-6">
                        <TextCompoent 
                        className = 'sub-headline'
                        headline = 'See An Overview Of All Your Stocked Products'
                        details = 'You will be able to see a complete overview of all the stocked products that you have along with the item code, buying / selling price, no. of stocks left, and more.'
                        />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Features;