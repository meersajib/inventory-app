import React from 'react';
import Logo from '../img/logo.png';

import Button from '../Components/Button';
import Input from '../Components/Input';

import fbImage from '../img/facebook.png';
import twtImage from '../img/twitter.png';
import ytbImage from '../img/youtube.png';
import gplusImage from '../img/google.png';

const Footer = () => {
    return(
        <div>
        <footer id="footer">
        <div className="container">
            <div className="row d-flex justify-content-md-center">
                <div className="col-md-3 ">
                    <img src={Logo} alt="" />
                    <h5>Newsletter</h5>
                    <Input 
                    inputType = 'text'
                    placeHolder = 'Email'
                    className = 'subscribe-input'
                    />
                    <Button 
                    className = 'btn subscribe-btn'
                    btnText = 'Subscribe'
                    />
                </div>
                <div className="col-md-3">
                    <h5 className="text-uppercase">wp erp</h5>
                    <ul>
                        <li>About</li>
                        <li>Refund Policy</li>
                        <li>Support Policy</li>
                        <li>FAQ</li>
                    </ul>
                </div>
                <div className="col-md-3">
                    <h5 className="text-uppercase">products</h5>
                    <ul>
                        <li>HRM</li>
                        <li>CRM</li>
                        <li>Accounting</li>
                        <li>Request A Demo</li>
                    </ul>
                </div>
                <div className="col-md-3">
                    <h5 className="text-uppercase">Resources</h5>
                    <ul>
                        <li>Documentation</li>
                        <li>Discussion</li>
                        <li>Forum</li>
                        <li>Submit Ideas</li>
                        <li>Terms of Service</li>
                        <li>Translate</li>
                        <li>Extensions</li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <footer id="footer-bottom">
        <div className="container">
            <div className="row d-flex justify-content-start align-items-center">
                <div className="col">
                    <div className="social-icon d-flex justify-content-start align-items-center">
                        <div className="social-item"><a href=""><img src={fbImage} /></a></div>
                        <div className="social-item"><a href=""><img src={twtImage} /></a></div>
                        <div className="social-item"><a href=""><img src={ytbImage} /></a></div>
                        <div className="social-item"><a href=""><img src={gplusImage} /></a></div>
                    </div>
                </div>
                <div className="col">
                    <p>© 2017 WP ERP. Built by <a href="https://www.upwork.com/o/profiles/users/_~01b1abe1a42d7db33e/">Meer Faridul Haque</a></p>
                </div>
            </div>
        </div>
    </footer>
    </div>
    )
}
export default Footer;