import React from 'react';

import TextComponent from '../Components/TextComponent';

import ShopingIcon from '../img/shoping-cart.png';
import ParcentIcon from '../img/parcent.png';


const Tax = () => {
    return(
        <section id="tax">
        <div className="container">
            <div className="row">
                <div className="col-lg-6 text-center tab-card">
                    <div className="tax-card">
                        <div className="d-flex justify-content-center align-items-center">
                            <img src={ShopingIcon} alt="" />
                        </div> 
                        <h5>Purchase</h5>
                    </div>
                    <div className="tax-card">
                        <div className="d-flex justify-content-center align-items-center">
                            <img src={ParcentIcon} alt="" />
                        </div>
                        <h5>Tax Rate</h5>
                    </div>
                </div>
                <div className="col-lg-6">
                        <TextComponent 
                        className = 'sub-headline'
                        headline = "Support For Automatic Tax Calculations"
                        details = "You have the option to calculate all the sales tax / pre-sales tax without any complications. Simply select the tax rate you configured and you are good to go"
                        />
                </div>
            </div>
        </div>
    </section>
    )
}

export default Tax;