import React, { Component } from 'react';
import Carousel from './Carousel/Carousel';
import Features from './Features/Features';
import Products from './Products/Products';
import Search from './Search/Search';
import Reports from './Reports/Reports';
import Tax from './Tax/Tax';
import Import from './Import/Import';
import Screenshot from './Screenshot/Screenshot';
import Demo from './Demo/Demo';
import Footer from './Footer/Footer';


class App extends Component {
  render() {
    return (
      <div>
        <Carousel />
        <Features />
        <Products />
        <Search />
        <Reports />
        <Tax />
        <Import />
        <Screenshot />
        <Demo />
        <Footer />
      </div>
    );
  }
}

export default App;
