import React from 'react';
import TextComponent from '../Components/TextComponent';
import curve from '../img/curve.png';

const Products = () => {
    return(
        <section id="products">
        <div className="container">
            <div className="row">
                <div className="col-lg-6 d-flex justify-content-md-end">
                        <TextComponent
                        className = 'sub-headline'
                        headline="Configure Each Product To Your Liking"
                        details="Inventory extension gives you the complete control over it’s customization. With it’ easy to
                        understand feature you can set everything just like you want."
                        />
                </div>
                <div className="col-lg-6 cards">

                    <div className="product-card">
                        <h4>Product Settings</h4>
                        <img className="neg-index" src={curve} alt="" />
                        <ul>
                            <li>Inventory item</li>
                            <li>Inventory asset account</li>
                            <li>140 - Inventory</li>
                            <li>item code</li>
                        </ul>
                    </div>
                    <div className="product-card">
                        <h4 className="text-danger">Purchase</h4>
                        <ul>
                            <li>
                                Cost Price
                                <div className="card-border"></div>
                            </li>
                            <li>
                                Purchase Account
                                <div className="card-border"></div>
                            </li>
                            <li>
                                Tax Rate
                                <div className="card-border"></div>
                            </li>
                            <li>
                                Purchase Description
                                <div className="card-border"></div>
                            </li>
                        </ul>
                    </div>
                    <div className="product-card">
                        <div className="card-border"></div>
                        <div className="card-border"></div>
                        <div className="card-border"></div>
                        <div className="card-border"></div>
                        <div className="card-border"></div>
                        <div className="card-border"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    )
}

export default Products;